/*
 *  Job to deploy melissadata components like address, geocoder and license
 */
def NOTIF_SUB = "${env.JOB_NAME}#${env.BUILD_NUMBER}"
def NOTIF_TXT = "${env.JOB_NAME}#${env.BUILD_NUMBER}\n${env.BUILD_URL}"
pipeline {
    agent {
        label 'deploy'
    }
    parameters {
        choice(
            name: 'ACTION',
            choices: ['STAGE', 'ENABLE'],
            description: 'Pick one'
        )
        choice(
            name: 'ENVIRONMENT_REPO',
            choices: ['environments-customer', 'environments-he'],
            description: 'Environments repo'
        )
        string(
            name: 'CUSTOMER',
            defaultValue: '<customer>',
            trim: true,
            description: 'Customer to be used'
        )
        string(
            name: 'ENVIRONMENT',
            defaultValue: '<environment>',
            trim: true,
            description: 'Environment to be used for install'
        )
        string(
            name: 'PLATFORM_TOOLS_BRANCH',
            defaultValue: 'master',
            trim: true,
            description: 'Branch for Platform'
        )
        booleanParam(
            name: 'DEPLOY_PATCH',
            defaultValue: 'true',
            description: 'To deploy patch as part of melissadata enable action'
        )
    }
    environment {
    JOB_NAME = "${env.JOB_NAME}"
    ACTION = "${params.ACTION}"
    PLATFORM_TOOLS_BRANCH = "${params.PLATFORM_TOOLS_BRANCH}"
    ENVIRONMENTS_BRANCH = "master"
    STAGE = "${params.STAGE}"
    ENVIRONMENTS_REPO = "${params.ENVIRONMENT_REPO}"
    CUST = "${params.CUSTOMER}"
    ENV = "${params.ENVIRONMENT}"
    CUST_ENV = "${CUST}/${ENV}"
    ENVDIR = "${WORKSPACE}/${ENVIRONMENT_REPO}/${CUST_ENV}"
    PLATFORM_TOOLS_DIR = "${WORKSPACE}/platform-tools/"
    ANSIBLE_CONFIG = "${PLATFORM_TOOLS_DIR}/ansible.cfg"
    }
    stages {
        stage ('Setup Platform-tools ') {
            steps {
                sh 'chmod go+w ${WORKSPACE}'
                echo "Cloning Platform-Tools Repo"
                sh 'sudo -E -u ansible -- sh -c "cd ${WORKSPACE} && git clone --branch ${PLATFORM_TOOLS_BRANCH} git@bitbucket.org:healthedge/platform-tools.git"'
                echo "Cloning Environments Repo"
                sh 'sudo -E -u ansible -- sh -c "cd ${WORKSPACE} && git clone --branch ${ENVIRONMENTS_BRANCH} git@bitbucket.org:healthedge/${ENVIRONMENT_REPO}.git"'

                wrap([$class: 'BuildUser']) {
                    script {
                        env['ENV_PROPS_DIR'] = sh(script: 'find ${ENVDIR} -name pushbutton.yml -exec dirname {} \\;', returnStdout: true).trim()
                        env['BUILD_USER'] = BUILD_USER
                        env['BUILD_USER_ID'] = BUILD_USER_ID
                        env['BUILD_USER_EMAIL'] = BUILD_USER_EMAIL.startsWith('a-') ? BUILD_USER_EMAIL.minus('a-') : BUILD_USER_EMAIL
                        currentBuild.description = "${BUILD_USER_ID} ==> ${ACTION} on ${CUST_ENV}"
                    }
                }

            }
        }
        stage ('Display Pre-deployment Summary') {
            steps {
                echo "Pre-deployment Summary"
                script {
                  echo "Displaying pre-deployment summary"
                  display_pre_deployment_summary()
                }
            }
        }
        stage ('Validate Job Inputs') {
            steps {
                script {
                    echo "Validating Job Input Parameters"
                    validate_job_parameters()
                }
            }
        }
        stage ('Staging melissadata components') {
            steps {
                script {
                    echo "Running the stage_melissadata.yml"
                    sh 'sudo -E -u ansible -- sh -c "cd ${PLATFORM_TOOLS_DIR}  && ansible-playbook -i ${ENV_PROPS_DIR}/hosts plays/melissadata/stage_melissadata.yml"'
                    }
            }
            when { expression { params.ACTION == 'STAGE' }
            }
        }
        stage ('Enable melissadata components') {
            steps {
                script {
                    env['PLAYBOOK_EXTRA_OPTS'] = (params.DEPLOY_PATCH) ? "-e \"deploy_md_patch=True\"" : ""
                    echo "Running the enable_melissadata.yml"
                    sh 'sudo -E -u ansible -- sh -c "cd ${PLATFORM_TOOLS_DIR}  && ansible-playbook -i ${ENV_PROPS_DIR}/hosts plays/melissadata/enable_melissadata.yml ${PLAYBOOK_EXTRA_OPTS}"'
                    }
            }
            when { expression { params.ACTION == 'ENABLE' }
            }
        }
    }
    post {
        success {
            echo "Notifying: Melissadata ${ACTION} completed for ${CUST_ENV}"
            slackSend channel: '#deploy-success', color: 'good', message: "SUCCESS: melissadata ${ACTION} on ${CUST_ENV} \n${NOTIF_TXT}"
            emailext subject: "SUCCESS: ${NOTIF_SUB}", body: "SUCCESS: SUCCESS: melissadata ${ACTION} on ${CUST_ENV} \n${NOTIF_TXT}", to: "${BUILD_USER_EMAIL}"

        }
        failure {
            echo "Notifying: install failed for ${CUST_ENV}"
            slackSend channel: '#deploy-failures',    color: 'bad', message: "FAILED: melissadata ${ACTION} on ${CUST_ENV} \n${NOTIF_TXT}"
            emailext subject: "FAILED: ${NOTIF_SUB}", body: "FAILED: SUCCESS: melissadata ${ACTION} on ${CUST_ENV} \n${NOTIF_TXT}", to: "${BUILD_USER_EMAIL}"
        }
        always {
            // Deleting all the files owned by ansible user
            sh '''
                sudo -E -u ansible -- sh -c "find ${WORKSPACE} -user ansible |sort -r |xargs rm -rf"
            '''
            // Deleting the whole workspace owned by Jenkins user
            deleteDir()
        }
    }
}
def validate_job_parameters(){
    def CUSTOMER_EXISTS = fileExists "${ENVIRONMENT_REPO}/${CUST}"
    def ENVIRONMENT_EXISTS = fileExists "${ENVDIR}"
    def ENVIRONMENT_PROPS_EXISTS = fileExists "${ENVDIR}/generated-payor-properties"
    if (CUSTOMER_EXISTS) {
        echo "Customer:  ${CUST}  validated"
    } else {
        error "Customer:  ${CUST} doesn't exist"
    }
    if (ENVIRONMENT_EXISTS && ENVIRONMENT_PROPS_EXISTS) {
        echo "Environment:  ${ENV}  validated"
    } else {
        error "Environment:  ${ENV}  doesn't exist"
    }
}

def display_pre_deployment_summary() {
    echo """
    ###################### Pre-deployment Summary ####################################
    CUSTOMER:                    ${CUST}
    ENVIRONMENT:                 ${ENV}
    ACTION TRIGGERED:            ${ACTION}
    TRIGGERED BY:                ${BUILD_USER_ID}
    PRODUCTS:                    Melissadata
    DEPLOY_PATCH:                ${params.DEPLOY_PATCH}
    ##################################################################################
    """
}
